function natSort(as, bs) {
  let a;
  let b;
  let a1;
  let b1;


  const rx = /(\d+)|(\D+)/g;
  const rd = /\d/;
  const
    rz = /^0/;
  if (typeof as === 'number' || typeof bs === 'number') {
    if (Number.isNaN(as)) return 1;
    if (Number.isNaN(bs)) return -1;
    return as - bs;
  }
  a = String(as).toLowerCase();
  b = String(bs).toLowerCase();
  if (a === b) return 0;
  if (!(rd.test(a) && rd.test(b))) return a > b ? 1 : -1;
  a = a.match(rx);
  b = b.match(rx);
  while (a.length && b.length) {
    a1 = a.shift();
    b1 = b.shift();
    if (a1 !== b1) {
      if (rd.test(a1) && rd.test(b1)) {
        return a1.replace(rz, '.0') - b1.replace(rz, '.0');
      }
      return a1 > b1 ? 1 : -1;
    }
  }
  return a.length - b.length;
}
export default {
  natSort,
};
