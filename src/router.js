import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Question from './components/Question';

Vue.use(Router);

export default new Router({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      redirect: '/radio/item/S-2',
    },
    {
      path: '/radio',
      name: 'radio',
      component: Home,
      children: [
        {
          path: 'item/:trackTitle',
          name: 'track-item',
          component: Home,
        },
      ],
    },
    {
      path: '/question',
      name: 'question',
      component: Question,
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
    },
  ],
});
