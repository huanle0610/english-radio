// github issue comment
// Copyright (C) 2017
// Joseph Pan <http://github.com/wzpan>
// This library is free software; you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; either version 2.1 of the
// License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301 USA
//


let type; let username; let repo; let client_id; let client_secret; let no_comment; let go_to_comment; let btn_class; let comments_target; let recent_comments_target; let
  loading_target;
const github_addr = 'https://github.com/';
const github_api_addr = 'https://api.github.com/repos/';
const oschina_addr = 'http://git.oschina.net/';
const oschina_api_addr = 'http://git.oschina.net/api/v5/repos/';
const spinOpts = {
  lines: 13,
  length: 10,
  width: 6,
  radius: 12,
  corners: 1,
  rotate: 0,
  direction: 1,
  color: '#5882FA',
  speed: 1,
  trail: 60,
  shadow: false,
  hwaccel: false,
  className: 'spinner',
  zIndex: 2e9,
  top: 'auto',
  left: '50%',
};

const _getComment = function _getComment(params, callback) {
  let comments = void 0;


  let comments_url = void 0;


  let page = void 0;

  // Get comments
  comments = params.comments;
  comments_url = params.comments_url;
  page = params.page;
  $.ajax({
    url: `${comments_url}?page=${page}`,
    dataType: 'json',
    cache: false,
    crossDomain: true,
    data: client_id && client_secret ? `client_id=${client_id}&client_secret=${client_secret}` : '',
    success: function success(page_comments) {
      if (!page_comments || page_comments.length <= 0) {
        callback && typeof callback === 'function' && callback(comments);
        callback = null;
        return;
      }
      page_comments.forEach((comment) => {
        comments.push(comment);
      });
      page += 1;
      params.comments = comments;
      params.page = page;
      _getComment(params, callback);
    },
    error: function error(err) {
      callback && typeof callback === 'function' && callback(comments);
      callback = null;
    },
  });
};

const _getCommentsUrl = function _getCommentsUrl(params, callback) {
  let issue_title = void 0;


  let page = void 0;
  let found = false;
  issue_title = params.issue_title;
  page = params.page;

  const api_addr = type == 'github' ? github_api_addr : oschina_api_addr;
  $.ajax({
    url: `${api_addr + username}/${repo}/issues?page=${page}`,
    dataType: 'json',
    cache: false,
    crossDomain: true,
    data: client_id && client_secret ? `client_id=${client_id}&client_secret=${client_secret}` : '',
    success: function success(issues) {
      if (!issues || issues.length <= 0) {
        callback && typeof callback === 'function' && callback('', '');
        callback = null;
        return;
      }
      issues.forEach((issue) => {
        // match title
        if (issue.title && issue.title == issue_title) {
          callback && typeof callback === 'function' && callback(issue.comments_url, issue);
          found = true;
          callback = null;
        }
      });
      if (!found) {
        page += 1;
        params.page = page;
        _getCommentsUrl(params, callback);
      }
    },
    error: function error() {
      callback && typeof callback === 'function' && callback('', '');
      callback = null;
    },
  });
};

const _getIssue = function _getIssue(issue_id, callback) {
  const api_addr = type == 'github' ? github_api_addr : oschina_api_addr;
  const issue_url = `${api_addr + username}/${repo}/issues/${issue_id}`;
  _getIssueByUrl(issue_url, (issue) => {
    callback && typeof callback === 'function' && callback(issue);
    callback = null;
  });
};

var _getIssueByUrl = function _getIssueByUrl(issue_url, callback) {
  $.ajax({
    url: issue_url,
    dataType: 'json',
    cache: false,
    crossDomain: true,
    data: client_id && client_secret ? `client_id=${client_id}&client_secret=${client_secret}` : '',
    success: function success(issues) {
      if (!issues || issues.length <= 0) {
        callback && typeof callback === 'function' && callback();
        callback = null;
        return;
      }
      const issue = issues;
      callback && typeof callback === 'function' && callback(issue);
      callback = null;
    },
    error: function error() {
      callback && typeof callback === 'function' && callback();
      callback = null;
    },
  });
};

const _renderComment = function _renderComment(comment) {
  const timeagoInstance = timeago();
  const user = comment.user;
  const content = marked(comment.body);
  const ago = timeagoInstance.format(comment.created_at);
  const current_user = user.login == username ? 'current-user' : '';
  const addr = type == 'github' ? github_addr : oschina_addr;
  const owner = user.login == username ? `\n        <span class="timeline-comment-label text-bold tooltipped tooltipped-multiline tooltipped-s" aria-label="${username} is the author of this blog.">\n        Owner\n    </span>\n        ` : '';
  return `\n        <div class="timeline-comment-wrapper js-comment-container">\n        <div class="avatar-parent-child timeline-comment-avatar">\n        <a href="${addr}${user.login}">\n        <img alt="@${user.login}" class="avatar rounded-1" height="44" src="${user.avatar_url}&amp;s=88" width="44">\n        </a>\n        </div>\n        <div id="issuecomment-310820108" class="comment previewable-edit js-comment js-task-list-container  timeline-comment js-reorderable-task-lists reorderable-task-lists ${current_user}" data-body-version="0ff4a390ed2be378bf5044aa6dc1510b">\n\n        <div class="timeline-comment-header">\n        ${owner}\n        <h3 class="timeline-comment-header-text f5 text-normal">\n\n        <strong>\n        <a href="${addr}${user.login}" class="author">${user.login}</a>\n        \n    </strong>\n\n    commented  \n\n        <a href="#issuecomment-${comment.id}" class="timestamp"><relative-time datetime="${comment.created_at}" title="${comment.created_at}">${ago}</relative-time></a>\n\n    </h3>\n        </div>\n        \n        <table class="d-block">\n        <tbody class="d-block">\n        <tr class="d-block">\n        <td class="d-block comment-body markdown-body js-comment-body">\n        ${content}\n    </td>\n        </tr>\n        </tbody>\n        </table>\n        </div>\n        </div>\n        `;
};

const _renderRecentComment = function _renderRecentComment(user, title, content, time, url, callback) {
  const addr = type == 'github' ? github_addr : oschina_addr;
  const res = `\n        <div class="comment-item">\n          <div class="row comment-widget-head">\n            <div class="xl-col-3 comment-widget-avatar">\n              <a href="${addr}${user.login}">\n                <img alt="@${user.login}" class="avatar rounded-1" height="44" src="${user.avatar_url}&amp;s=88" width="44">\n              </a>\n            </div>\n            <div class="comment-widget-body">\n              <span><a class="comment-widget-user" href="${addr}${user.login}" target="_blank">${user.login}</a> </span>\n              <div class="comment-widget-content">${content}</div>\n            </div>\n          </div>\n          <br/>\n          <div class="comment-widget-meta">\n            <span class="comment-widget-title">${title}</span> | <span class="comment-widget-date">${time}</span>\n          </div>\n        </div>\n        `;
  callback && typeof callback === 'function' && callback(res);
  callback = null;
};

const _getRecentCommentList = function _getRecentCommentList(comment_list, i, render_count, total_count, comments, callback) {
  if (render_count >= total_count || i >= comments.length) {
    callback && typeof callback === 'function' && callback(comment_list);
    callback = null;
    return;
  }
  const comment = comments[i];
  if (!comment) return;
  const content = marked(comment.body);
  const title = comment.title;
  const user = comment.user;
  const timeagoInstance = timeago();
  const time = timeagoInstance.format(comment.created_at);
  const url = comment.html_url;
  if (!content || content == '') {
    i++;
    _getRecentCommentList(comment_list, i, render_count, total_count, comments, callback);
    callback = null;
    return;
  }
  if (!title) {
    // Get title of issue
    _getIssueByUrl(comment.issue_url, (issue) => {
      _renderRecentComment(user, issue.title, content, time, url, (item) => {
        comment_list += item;
        i++;
        render_count++;
        _getRecentCommentList(comment_list, i, render_count, total_count, comments, callback);
      });
    });
  } else {
    _renderRecentComment(user, title, content, time, url, (item) => {
      comment_list += item;
      i++;
      render_count++;
      _getRecentCommentList(comment_list, i, render_count, total_count, comments, callback);
    });
  }
};

const _renderRecentCommentList = function _renderRecentCommentList(comments, count) {
  const i = 0;
  const render_count = 0;
  const comment_list = '';
  _getRecentCommentList(comment_list, i, render_count, count, comments, (comment_list) => {
    $(recent_comments_target).append(comment_list);
  });
};

const _renderHTML = function _renderHTML(params) {
  let issue = void 0;


  let comments = void 0;


  let comments_url = void 0;


  let issue_title = void 0;
  issue = params.issue;
  comments = params.comments;
  comments_url = params.comments_url;
  issue_title = params.issue_title;

  const addr = type == 'github' ? github_addr : oschina_addr;
  const api_addr = type == 'github' ? github_api_addr : oschina_api_addr;
  if ((!issue || !issue.body || issue.body == '') && (!comments || comments.length <= 0)) {
    const _res = `\n            <div class='js-discussion no-comment'>\n            <span>${no_comment}</span>\n            </div>\n            `;
    $(comments_target).append(_res);
  } else {
    let _res2 = '\n            <div class="discussion-timeline js-quote-selection-container">\n            <div class="js-discussion js-socket-channel">\n            ';
    if (issue && issue.body && issue.body != '') {
      _res2 += _renderComment(issue);
    }
    comments.forEach((comment) => {
      _res2 += _renderComment(comment);
    });
    _res2 += '</div></div>';
    $(comments_target).append(_res2);
  }
  let issue_url = void 0;
  if (!comments_url) {
    issue_url = `${addr}/${username}/${repo}/issues/new?title=${issue_title}#issue_body`;
  } else {
    issue_url = `${comments_url.replace(api_addr, addr).replace('comments', '')}#new_comment_field`;
  }
  const res = `\n        <p class="goto-comment">\n        <a href="${issue_url}" class="${btn_class}" target="_blank">${go_to_comment}</a>\n        </p>\n        `;
  $(comments_target).append(res);
};

const CompareDate = function CompareDate(a, b) {
  const d1 = a.created_at.replace('T', ' ').replace('Z', '').replace(/-/g, '\/');
  const d2 = b.created_at.replace('T', ' ').replace('Z', '').replace(/-/g, '\/');
  return new Date(d1) > new Date(d2);
};

const _getRecentIssues = function _getRecentIssues(params, callback) {
  let count = void 0;
  count = params.count;

  const api_addr = type == 'github' ? github_api_addr : oschina_api_addr;
  $.ajax({
    url: `${api_addr + username}/${repo}/issues?per_page=100&sort=created&direction=desc`,
    dataType: 'json',
    cache: false,
    crossDomain: true,
    data: client_id && client_secret ? `client_id=${client_id}&client_secret=${client_secret}` : '',
    success: function success(issues) {
      if (issues.length > count) {
        if (navigator.userAgent.indexOf('MSIE') != -1 || navigator.userAgent.indexOf('Edge') != -1 || !!document.documentMode == true) {
          issues = issues.sort(CompareDate).slice(0, 5);
        } else {
          issues = issues.sort(CompareDate).reverse().slice(0, 5);
        }
      }
      callback && typeof callback === 'function' && callback(issues);
      callback = null;
    },
    error: function error(err) {
      callback && typeof callback === 'function' && callback();
      callback = null;
    },
  });
};

const _getRecentComments = function _getRecentComments(params, callback) {
  let count = void 0;
  count = params.count;

  const api_addr = type == 'github' ? github_api_addr : oschina_api_addr;
  $.ajax({
    url: `${api_addr + username}/${repo}/issues/comments?per_page=100&sort=created&direction=desc`,
    dataType: 'json',
    cache: false,
    crossDomain: true,
    data: client_id && client_secret ? `client_id=${client_id}&client_secret=${client_secret}` : '',
    success: function success(comments) {
      if (comments.length > count) {
        if (navigator.userAgent.indexOf('MSIE') != -1 || navigator.userAgent.indexOf('Edge') != -1 || !!document.documentMode == true) {
          comments = comments.sort(CompareDate).slice(0, 5);
        } else {
          comments = comments.sort(CompareDate).reverse().slice(0, 5);
        }
      }

      callback && typeof callback === 'function' && callback(comments);
      callback = null;
    },
    error: function error(err) {
      callback && typeof callback === 'function' && callback();
      callback = null;
    },
  });
};

const getRecentCommentsList = function getRecentCommentsList(params) {
  let count = void 0;


  let user = void 0;
  type = params.type;
  user = params.user;
  repo = params.repo;
  client_id = params.client_id;
  client_secret = params.client_secret;
  count = params.count;
  recent_comments_target = params.recent_comments_target;

  username = user;
  recent_comments_target = recent_comments_target || '#recent-comments';
  let recentList = new Array();
  // Get recent issues and comments and filter out 10 newest comments
  _getRecentIssues(params, (issues) => {
    recentList = recentList.concat(issues);
    _getRecentComments(params, (comments) => {
      recentList = recentList.concat(comments);
      if (navigator.userAgent.indexOf('MSIE') != -1 || navigator.userAgent.indexOf('Edge') != -1 || !!document.documentMode == true) {
        recentList = recentList.sort(CompareDate);
      } else {
        recentList = recentList.sort(CompareDate).reverse();
      }
      _renderRecentCommentList(recentList, count);
    });
  });
};

const getComments = function getComments(params) {
  let issue_title = void 0;


  let issue_id = void 0;


  let user = void 0;
  type = params.type;
  user = params.user;
  repo = params.repo;
  client_id = params.client_id;
  client_secret = params.client_secret;
  no_comment = params.no_comment;
  go_to_comment = params.go_to_comment;
  issue_title = params.issue_title;
  issue_id = params.issue_id;
  btn_class = params.btn_class;
  comments_target = params.comments_target;
  loading_target = params.loading_target;

  comments_target = comments_target || '#comment-thread';
  username = user;
  const spinner = new Spinner(spinOpts);
  const timeagoInstance = timeago();
  let comments_url;
  const comments = new Array();
  type = type || 'github';
  btn_class = btn_class || 'btn';

  loading_target && spinner.spin($(`div${loading_target}`).get(0));
  if (!issue_id || issue_id == 'undefined' || typeof issue_id === 'undefined') {
    _getCommentsUrl({
      issue_title,
      page: 1,
    }, (comments_url, issue) => {
      if (comments_url != '' && comments_url != undefined) {
        _getComment({
          comments,
          comments_url,
          page: 1,
        }, (comments) => {
          loading_target && spinner.spin();
          _renderHTML({
            issue,
            comments,
            comments_url,
            issue_title,
          });
        });
      } else {
        loading_target && spinner.spin();
        _renderHTML({
          issue,
          comments,
          comments_url,
          issue_title,
        });
      }
    });
  } else {
    const api_addr = type == 'github' ? github_api_addr : oschina_api_addr;
    const _comments_url = `${api_addr + username}/${repo}/issues/${issue_id}/comments`;
    _getIssue(issue_id, (issue) => {
      _getComment({
        comments,
        comments_url: _comments_url,
        page: 1,
      }, (comments) => {
        loading_target && spinner.spin();
        _renderHTML({
          issue,
          comments,
          comments_url: _comments_url,
          issue_title,
        });
        loading_target && spinner.spin();
      });
    });
  }
};
